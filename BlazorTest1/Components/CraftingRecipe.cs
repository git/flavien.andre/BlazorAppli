﻿using BlazorTest1.Models;
public class CraftingRecipe
{
    public Item Give { get; set; }
    public List<List<string>> Have { get; set; }
}