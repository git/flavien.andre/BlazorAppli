﻿using BlazorTest1.Models;
namespace BlazorTest1.Components
{
    public class CraftingAction
    {
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}